$(function () {
    var APPLICATION_ID = "A73C62C9-16BC-F105-FFDB-AE6B5C028100",
    SECRET_KEY = "905C3CAF-1449-7213-FF23-B205A714A000",
    VERSION = "v1";
    
   Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    var postsCollection = Backendless.Persistence.of(Post).find();
    
    console.log(postsCollection);
    
    var wrapper = {
        posts: postsCollection.data
    };
    
    Handlebars.registerHelper('format', function (time) {
        return moment(time).format('dddd, MMM Do YYYY');
    });
    
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
    
    $('.main-container').html(blogHTML);

});

function Post(args) {
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}

$(document).on('click', '.deleteA',function (event){
 Materialize.toast('Deleted', 1500);
 Backendless.Persistence.of(Post).remove(event.target.attributes.data.nodeValue);
 //setTimeout(location.reload(),100000000000);
});

$(document).on('click', '.checkmark',function (event){
 Materialize.toast('Competed', 1500);
});

var taskStorage = Backendless.Persistence.of( Tasks);
   var dataQuery = {
        condition: "author = " + YOUR_ID_HERE
   };
   var myTasks = taskStorage.find( dataQuery );